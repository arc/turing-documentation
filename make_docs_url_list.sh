#!/bin/bash

# Define the output file
OUTPUT_FILE="combined_docs.md"

# Clear the output file if it already exists
> "$OUTPUT_FILE"

# Add a title to the combined Markdown document
echo "# Combined Documentation" >> "$OUTPUT_FILE"
echo "" >> "$OUTPUT_FILE"

# Find all .md files specifically in the /docs subdirectory
find ./docs -type f -name "*.md" | while read -r file; do
    # Add a header for each file to the combined Markdown document
    echo "## $(basename "$file")" >> "$OUTPUT_FILE"
    echo "" >> "$OUTPUT_FILE"
    
    # Append the contents of the file to the output file
    cat "$file" >> "$OUTPUT_FILE"
    echo "" >> "$OUTPUT_FILE"
    echo "---" >> "$OUTPUT_FILE"  # Add a horizontal rule for separation
    echo "" >> "$OUTPUT_FILE"
done

# Print completion message
echo "Combined Markdown document created: $OUTPUT_FILE"
