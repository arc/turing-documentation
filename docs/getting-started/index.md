# Getting Started with Turing

??? note "TLDR"
    [Request an account](https://arc.wpi.edu/computing/accounts/turing-accounts/) and then `ssh username@turing.wpi.edu` from the wpi netowrk.


## Requesting a Turing Account

If you need to access Turing for a class, your teacher should request accounts for everyone in the class. To access Turing as an individual, request an account by completing the [Turing Account Request](https://arc.wpi.edu/computing/accounts/turing-accounts/) form. Once your account is confirmed, you can connect to Turing.



## Connecting to the Turing Cluster

To use Turing, you can connect via [**OnDemand**](https://ondemand.turing.wpi.edu/) by going to Clusters/Turing Shell Access or by **SSH** (Secure Shell). When connecting via ssh ensure you are connected to WPI’s network or use the [**GlobalProtect VPN**](https://hub.wpi.edu/software/570/globalprotect.) if off-campus.  

### Prerequisites

Before connecting:

- **Install an SSH Client**:
    - **Windows**: Use he built-in OpenSSH client (available in Windows 10+) or a third party client like [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) or [MobaXterm](https://mobaxterm.mobatek.net/).
    - **macOS and Linux**: SSH is typically pre-installed.
  
!!! warning "macOS Sequoia and later:" 
    Follow the [Disable Private Address for
    iOS instructions](https://help.wpi.edu/kb?id=487) to disable this setting on
    your Mac. Private MAC address prevents your Mac from connecting to Turing,
    so it must be disabled before attempting to connect with SSH.

### Steps to Connect

1. **Open Your Terminal or SSH Client**

    >**Windows:** For windows 10 and later open the command prompt or power shell application. for older versions open PuTTY or MobaXterm.

    >**macOS and Linux:** Use the Terminal application.

2. **Connect to Turing**

    Use the following command, replacing `gompei` with your WPI username:

    ```bash
    ssh gompei@turing.wpi.edu
    ```

    > press enter/return to enter commands

3. **Authenticate**

    >**Server Key Prompt** Accept the server's SSH key if prompted (type `yes` and press Enter).

    >**Password Prompt** Enter your WPI password.

4. **Successful Connection**

    >After authentication, you will be connected to a turing login node.

5. **Exiting**

    >You can exit a session by typing `exit` and then enter/return. You can also use the shortcut ctrl-D
