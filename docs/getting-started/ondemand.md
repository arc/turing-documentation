# Using OpenOnDemand

??? note "TLDR"
    [Access Turing OnDemand](https://ondemand.turing.wpi.edu/pun/sys/dashboard) to manage files, launch a terminal, manage jobs and run experimental GUI app. 

## Accessing Turing via Open OnDemand  
If standard access to Turing is unavailable, you can use Open OnDemand. This method even allows you to access Turing conveniently from your phone! It's a fast and easy way to check on your remote work.

### Accessing the Dashboard

- Go to the [Turing OnDemand Dashboard](https://ondemand.turing.wpi.edu/pun/sys/dashboard).
- Ensure you have an active Turing account and are connected to WPI’s network or VPN.

### Managing Files
 Click on Files and then Home directory. From here you can click and drag files to and from your computer.

### Launching a Terminal

- Click "Clusters" > "Turing Shell Access"

<!-- ### Running a Desktop Session

- Click "Interactive Apps" > "Desktop."
- Select resources and session time, then click "Launch."
- Once ready, click "Connect" to access a graphical desktop.

This is currently broken
 -->
### Managing Jobs

- **Submit Jobs**: Use the "Job Composer" to create and submit job scripts.
- **Monitor Jobs**: Check "Active Jobs" to view running jobs.
