# Apptainer, Docker, and Containers

??? note "TLDR"
    Turing uses *Apptainer* as its container system. After `module load apptainer` you can load perbuilt docker images using `apptainer  run --userns  docker://organization/container:version`. Turing does **NOT** support docker directly.

## What is a Container?

A **container** is a portable software package that includes everything needed to run an application: code, libraries, and dependencies. It ensures consistency across environments, making it ideal for running software with specific requirements.

---

## When to Use a Container

If the software you need is not available as a module on the Turing cluster, consider using a container. Containers provide flexibility by isolating the software environment, allowing you to run applications without relying on system-wide installations. 

---

## Using Containers on Turing

The Turing cluster uses **Apptainer** to run containers. It supports Docker containers and is optimized for HPC environments.

### Running Prebuilt Containers

You can often find the software you need as a prebuilt container on platforms like [Docker Hub](https://hub.docker.com). Follow these steps:

1. **Find the Container**  
   Search for the container on Docker Hub. The container address is included in the `docker pull` command (e.g., `docker://organization/container:version`).

2. **Run the Container**  
   Apptainer can directly run containers available on DockerHub. For example, to
run the sylabsio/lolcow:latest container, first run

```bash
module load apptainer
```


Then use the following command to save a public Docker container as a SIF file for later use with Apptainer:

```bash
$ apptainer pull docker://sylabsio/lolcow:latest
```

```bash
INFO:    Converting OCI blobs to SIF format
INFO:    Starting build...
Getting image source signatures
Copying blob 16ec32c2132b done  
Copying blob 5ca731fc36c2 done  
Copying config fd0daa4d89 done  
Writing manifest to image destination
Storing signatures
2024/07/17 14:25:43  info unpack layer: sha256:16ec32c2132b43494832a05f2b02f7a822479f8250c173d0ab27b3de78b2f058
2024/07/17 14:25:44  info unpack layer: sha256:5ca731fc36c28789c5ddc3216563e8bfca2ab3ea10347e07554ebba1c953242e
INFO:    Creating SIF file...
```

This will save a file `lolcow_latest.sif ` which you can run using 


```bash
$ apptainer run --userns lolcow_latest.sif
```

```bash
 ______________________________
< Wed Jul 17 14:25:57 EDT 2024 >
 ------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

More information about using Docker containers with Apptainer can be found
at the [Apptainer
documentation](https://apptainer.org/docs/user/main/docker_and_oci.html).
