# Managing Software on the Turing Cluster
??? note "TLDR"
    To load software on turing, use the module system. The command `module avail <name>` will search, and `module load <name>` will load it in to you environment.

## You Can't Install Your Own Software

On the Turing cluster, users are not permitted to install software system-wide. This is to ensure:

- **🔒 Security**: Prevent unauthorized or harmful software from affecting the cluster.
- **⚙️ Stability**: Maintain a consistent environment for all users.
- **📈 Performance**: Avoid conflicts between different software versions.

But don't worry! There are ways to use the software you need.

---

## Using the Module System

The cluster uses a **module system** to manage software. Modules allow you to load and unload different software packages easily within your session or job submission scripts.

### Listing Available Modules

To search avaialable modules  use:

```bash
module avail <name> #(e.g., `module avail python`).
```

This will display a list of software packages you can use.

??? note "Tip"
    To display all avaialable modules use `module avail` with no name. This list is very long.


---
### Loading Modules

To load a module on the Turing cluster, use the `module load` command followed by the module name. For example:

```bash
module load python/3.8.13
```

This adds the software and its dependencies to your environment, allowing you to use it in your session.

### Key Commands for Managing Modules
- **Load a module**: `module load <module_name>`  
- **Check loaded modules**: `module list`  
- **Unload a module**: `module unload <module_name>` 

Always ensure you load the correct versions of modules required for your workflow, as conflicting versions may lead to errors. **Modules are session-specific.** You must reload them in every new session unless added to your initialization file `.bashrc`.

??? tip "Login and Compute nodes"
    Any modules loaded on the login node will be loaded when running a job on a compute node.



### Loading Modules in SLURM Job Scripts

To use a software package during a SLURM job, it is best to load modules directly within your job submission script.

Here’s an example of how to structure a submission script to load commonly used software modules:

```bash
#!/bin/bash
#SBATCH --job-name=my_job
#SBATCH --time=01:00:00
#SBATCH --mem=4G

# Load the necessary modules
module load gcc/12.1.0/
module load python/3.8.13/

# Run your application
python my_script.py
```

By including `module load` commands in your script, the environment will be set up for the job, ensuring the required software is available.


You can verify loaded modules with:

```bash
module list
```

And unload them when no longer needed:

```bash
module unload <module_name>
```