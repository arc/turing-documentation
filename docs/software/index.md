# Using Software Packages

!!! note

    Turing does not use the same software package management toolchains you
    might find on a typical Linux desktop. As such, commands like `apt install`,
    `yum install`, `dnf install`, and other similar system-level package
    managers will not work on Turing. Do not attempt to use them.

## Using modules

Turing provides access to hundreds of versions of software, accessible by loading them, such as:

- MATLAB
- ANSYS
- Abaqus

To view available modules, run the following command in your Turing terminal:

```bash
module avail [search]
```

Once you determine which software modules you would like to use, you can load them using:

```bash
module load <module name>
```

For example, to load MATLAB

```bash
module load matlab
```

Which edits the environment variables in your Turing session to make the loaded
modules available. You can put module load lines after the `#SBATCH` lines in your
SLURM script to ensure your job requests access to the correct software.

If a software module you would like to use is unavailable or you would like
access to a newer version that has yet to be installed, please make a request to
ARC by sending an email to [ARCweb@wpi.edu](mailto:ARCweb@wpi.edu). Modules are
the standard method for loading software packages on Turing.

## Using Apptainer

