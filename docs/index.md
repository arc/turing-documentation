---
hide:
  - navigation
  - toc
---

# Welcome to Turing


## 🖥️ What is Turing?

The Turing HPC cluster lets you process large datasets and run intensive computational tasks on powerful hardware. It uses many interconnected nodes to handle jobs beyond the capacity of standard computers. Learn about Turing's [hardware here](help/index.md#hardware-summary).

Users from across WPI submit job scripts specifying the needed resources. A scheduler manages the queue, ensuring fair and efficient processing. Connect to Turing via the login node and submit your scripts for execution.

---

## 🤔 Why Use Turing?

- **🚀 Performance**: Speed up jobs with parallel computing.  
- **📊 Scalability**: Handle datasets from hundreds of GB to TB.  
- **🔧 Specialized Resources**: Access GPUs, high-memory nodes, and pre-installed tools.  
- **⚙️ Flexibility**: Use software modules, containers, or custom Python environments.  

---

## 📘 **How to use Turing**
If you're new to Linux or HPC, start with the **[step-by-step guide](getting-started/)**.   

## 🛠️ **Quick Access Guide**  
If you're experienced with Linux and HPC, skip ahead to the [Quick Access Guide](getting-started/quick-access.md).  


