# Public Documentation

The following public documentation sites are useful for Turing:

- [SLURM](https://slurm.schedmd.com/documentation.html)
- [Apptainer](https://apptainer.org/documentation/)
- [Command line for beginners](https://ubuntu.com/tutorials/command-line-for-beginners)
