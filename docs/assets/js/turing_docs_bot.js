const scriptContent = `
import Chatbot from "https://cdn.jsdelivr.net/npm/flowise-embed@1.3.14/dist/web.js"
Chatbot.init({
    chatflowid: "f3e419d8-319d-4f91-8905-8d8b186cba20",
    apiHost: "https://arc-ai-rag-01.wpi.edu",
    chatflowConfig: {
        // topK: 2
    },
    theme: {
        button: {
            backgroundColor: "#3F51B5",
            right: 20,
            bottom: 20,
            size: "large",
            iconColor: "#eee",
        },
        chatWindow: {
            welcomeMessage: "This bot has access to these docs as well as basic information on SLURM and linux. Even still it will occasionaly hallucinate.",
            backgroundColor: "#ccc",
            height: 700,
            width: 400,
            fontSize: 16,
            poweredByTextColor: "#303235",
            botMessage: {
                backgroundColor: "#aaa",
                textColor: "#303235",
                showAvatar: true,
                avatarSrc: "https://cdn0.iconfinder.com/data/icons/phosphor-regular-vol-4/256/robot-512.png",
            },
            userMessage: {
                backgroundColor: "#3F51B5",
                textColor: "#eeeeee",
                showAvatar: true,
                avatarSrc: "https://cdn.iconscout.com/icon/free/png-512/free-user-icon-download-in-svg-png-gif-file-formats--account-profile-ui-basic-needs-pack-interface-icons-528036.png?f=webp&w=256",
            },
            textInput: {
                placeholder: "Type your question",
                backgroundColor: "#eeeeee",
                textColor: "#303235",
                sendButtonColor: "#3B81F6",
            }
        }
    }
});
`;

const scriptElement = document.createElement("script");
scriptElement.type = "module";
scriptElement.textContent = scriptContent;
document.body.appendChild(scriptElement);

// <a href="https://www.flaticon.com/free-icons/user" title="user icons">User icons created by Freepik - Flaticon</a>
// <a href="https://www.flaticon.com/free-icons/chatbot" title="chatbot icons">Chatbot icons created by Freepik - Flaticon</a>